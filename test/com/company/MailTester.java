package com.company;

import com.company.models.Mail;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.FileNotFoundException;

@RunWith(DataProviderRunner.class)
public class MailTester extends BaseTest {

    @Test
    @UseDataProvider("getMailList")
    public void sendMail(Object mail) {
        app.getMailHelper().createMail((Mail) mail);
    }

    @DataProvider
    public static Object[][] getMailList() throws FileNotFoundException {
        return MailReader.getMailList();
    }
}
