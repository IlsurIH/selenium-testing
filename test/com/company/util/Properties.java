package com.company.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Properties {

    private Map<String, String> properties;
    private boolean fileExists;
    private String path;

    {
        File file = new File(getClass().getClassLoader().getResource("").getFile().replace("%20", " "));
        path = file.getPath();
    }

    public Properties() {
        properties = new HashMap<>();

        File file = new File("resources/properties");
        if (file.exists() && file.isFile()) {
            fileExists = true;
            try {
                Scanner scanner = new Scanner(file);
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    String[] strs = line.split("=");
                    properties.put(strs[0], strs[1]);
                }
                scanner.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            fileExists = false;
        }
    }

    public String getLogin() {
        return getProp("login");
    }

    public String getMail() {
        return getProp("mail");
    }

    public String getPassword() {
        return getProp("password");
    }

    public String getProp(String name) {
        if (fileExists && properties.get(name) != null) {
            return properties.get(name);
        } else {
            return null;
        }
    }
}