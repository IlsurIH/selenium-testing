package com.company.helpers;

import com.company.ApplicationManager;
import com.company.Constants;

public class NavigationHelper extends Helper {
    public NavigationHelper(ApplicationManager manager) {
        super(manager);
    }

    public void openMainPage() {
        openUrl(Constants.BASE_URL);
    }

    public void goToMailWindow() {
        getButtonByText("Написать").click();
    }
}
