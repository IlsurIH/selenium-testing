package com.company;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class WebDriver extends FirefoxDriver {
    @Override
    protected void startSession(Capabilities desiredCapabilities) {
        String sid = getPreviousSessionIdFromSomeStorage();
        if (sid != null) {
            setSessionId(sid);
            try {
                getCurrentUrl();
            } catch (WebDriverException e) {
                sid = null;
            }
        }
        if (sid == null) {
            super.startSession(desiredCapabilities);
            saveSessionIdToSomeStorage(getSessionId().toString());
        }
    }

    private void saveSessionIdToSomeStorage(String id) {
        try {
            FileWriter session = new FileWriter(new File("session"));
            session.write(id);
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getPreviousSessionIdFromSomeStorage() {
        try {
            Scanner cc = new Scanner(new File("session"));
            String val = "";
            while (cc.hasNext())
                val += cc.next();
            return val;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
