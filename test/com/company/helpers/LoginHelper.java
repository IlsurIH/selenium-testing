package com.company.helpers;

import com.company.ApplicationManager;
import com.company.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;


public class LoginHelper extends Helper {

    public LoginHelper(ApplicationManager app) {
        super(app);
    }

    public void login() {
        Properties properties = app.getProperties();
        app.getNavigationHelper().openMainPage();

        //expand list of users
        WebElement loginField = getObjectByLabel("Логин");
        loginField.click();
        //choose user
        try {
            getObjectByText("div", properties.getLogin()).click();
        } catch (NoSuchElementException e) {
            loginField.sendKeys(properties.getMail());
        }

        WebElement passwordField = getObjectByLabel("Пароль");
        passwordField.clear();
        passwordField.sendKeys(properties.getPassword());
        getButtonByText("Вход").click();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        //wait until version loads
        WebDriverWait wait = new WebDriverWait(driver, 70);
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                try {
                    webDriver.findElement(By.xpath("(//div[contains(text(),'Версия ')])"));
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }
        });
    }
}
