package com.company;

import com.company.models.Mail;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class Generator {
    static final private SecureRandom random = new SecureRandom();
    static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    static String[] mails;

    public static void main(String[] args) throws IOException {
        int num = checkInput(args);
        mails = new String[args.length -1];
        System.arraycopy(args, 1, mails, 0, args.length - 1);
        List<Mail> list = new ArrayList<>(num);
        for (int i = 0 ; i < num; i ++) {
            Mail mail = new Mail(mails[i%mails.length], nextRandomString(), nextRandomString());
            list.add(mail);
        }
        String data = gson.toJson(list);
        File output = new File("output.json");
        new FileWriter(output).append(data).close();
    }

    private static int checkInput(String[] args) {
        try {
            assert args.length > 1;
            return Integer.parseInt(args[0]);
        } catch (Exception e) {
            throw new RuntimeException("Wrong input! Format is: 1 number, list of mails to send to!\nExample: 4 aa@ss.s ab@ss.s ba@ss.s bb@ss.s", e);
        }
    }

    public static String nextRandomString() {
        return new BigInteger(130, random).toString(32);
    }
}
