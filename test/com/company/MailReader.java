package com.company;

import com.company.models.Mail;
import com.google.gson.Gson;
import com.tngtech.java.junit.dataprovider.DataProvider;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MailReader {

    private static final File source = new File("output.json");

    @DataProvider
    public static Object[][] getMailList() throws FileNotFoundException {
        Scanner cc = new Scanner(source);
        String val = "";
        while (cc.hasNext())
            val += cc.next();

        Mail[] mails  =  new Gson().fromJson(val, Mail[].class);
        Mail[][] ret = new Mail[mails.length][2];
        for (int i =0 ; i < mails.length; i++) {
            ret[i] = new Mail[]{mails[i]};
        }
        return ret;
    }
}
