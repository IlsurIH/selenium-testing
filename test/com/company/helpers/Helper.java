package com.company.helpers;

import com.company.ApplicationManager;
import org.openqa.selenium.*;

import java.util.NoSuchElementException;
import java.util.function.Function;

public class Helper {
    protected final ApplicationManager app;
    protected WebDriver driver;
    private boolean acceptNextAlert = true;

    public Helper(ApplicationManager manager) {
        this.app = manager;
        driver = manager.getDriver();
    }

    protected boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    protected String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

    protected WebElement getButtonByText(final String text) {
        return getObjectByText("button", text);
    }

    protected WebElement getObjectByText(final String htmlTag, final String text) {
        return driver.findElement(By.xpath("(//" + htmlTag + "[contains(text(),'" + text + "')])"));
    }

    protected WebElement getObjectByLabel(final String text) {
        WebElement label = driver.findElement(By.xpath("(//label[contains(text(),'" + text + "')])"));
        return driver.findElement(By.id(label.getAttribute("for")));
    }

    protected void fillLabel(String label, String text) {
        getObjectByLabel(label).sendKeys(text);
    }

    protected WebDriver switchToIFrameByTitle(String name) {
        return driver.switchTo().frame(driver.findElement(By.xpath(".//iframe[contains(@title, '" + name + "')]")));
    }

    protected void doAfterSwitchToIFrameByTitle(String name, Function<WebDriver, WebDriver> work) {
        work.apply(switchToIFrameByTitle(name)).switchTo().defaultContent();
    }

    protected void openUrl(String string) {
        driver.get(string);
    }

}
