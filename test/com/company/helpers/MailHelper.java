package com.company.helpers;

import com.company.ApplicationManager;
import com.company.models.Mail;
import org.openqa.selenium.By;

public class MailHelper extends Helper {
    public MailHelper(ApplicationManager manager) {
        super(manager);
    }

    public void createMail(Mail mail) {
        app.getNavigationHelper().openMainPage();
        app.getNavigationHelper().goToMailWindow();
        fillMailWindow(mail);
        submit();
    }

    private void submit() {
        getButtonByText("Отправить").click();
    }

    private void fillMailWindow(final Mail mail) {
        fillLabel("Кому:", mail.getTo());
        fillLabel("Тема:", mail.getSubject());
        doAfterSwitchToIFrameByTitle("Содержание", webDriver -> {
            webDriver.findElement(By.tagName("body")).sendKeys(mail.getText());
            return webDriver;
        });
    }

}
