package com.company;

import com.company.helpers.LoginHelper;
import com.company.helpers.MailHelper;
import com.company.helpers.NavigationHelper;
import com.company.util.Properties;

import java.util.concurrent.TimeUnit;

public class ApplicationManager {
    private static WebDriver driver;
    private NavigationHelper navigationHelper;
    private LoginHelper loginHelper;

    protected Properties properties = new Properties();
    private MailHelper mailHelper;

    public ApplicationManager() {
        driver = new WebDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        navigationHelper = new NavigationHelper(this);
        loginHelper = new LoginHelper(this);
        mailHelper = new MailHelper(this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void stop() {
        driver.quit();
    }

    public LoginHelper getLoginHelper() {
        return loginHelper;
    }

    public NavigationHelper getNavigationHelper() {
        return navigationHelper;
    }

    public Properties getProperties() {
        return properties;
    }

    public MailHelper getMailHelper() {
        return mailHelper;
    }
}